#!/bin/sh
#  
#  Copyright 2015 Karl Lindén <lilrc@users.sourceforge.net>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

# features to probe for
probe=(
    "assert"
    "examples"
    "doc"
    "nls"
    "werror"
)

cflags=(
    "-Wall"
    "-Wconversion"
    "-Winline"
    "-Wmissing-declarations"
    "-Wshadow"
    "-Wtype-limits"
    "-pedantic"
)

bold="\e[1m"
normal="\e[0m"

default="\e[39m"
red="\e[91m"

workdir=$(pwd)
aclocaldir="${workdir}"/aclocal

# a counter to ensure the build variables do not collide
counter=-1

atexit() {
    try find "${workdir}/"{gop,cotesdex,gop-cotesdex} -type f -execdir chmod u+w {} +
}

die() {
    echo -e "${red}${bold}${@}${normal}${default}"
    atexit
    exit 1
}

run() {
    echo -e "${bold}${@}${normal}"
    "${@}" || die "${@} failed"
}

try() {
    echo -e "${bold}${@}${normal}"
    "${@}" || echo "${@} failed"
}

qrun() {
    echo -e "${bold}${@}${normal}"
    "${@}" > /dev/null || die "${@} failed"
}

xmkdir() {
    local dir=${1}
    if test ! -d "${dir}"
    then
        run mkdir "${dir}"
    fi
}

xmake() {
    run make ${MAKEOPTS} "${@}"
}

# USAGE: [args...] -- [next packages...]
build_real() {
    local args=()
    while test "${1}" != "--"
    do
        args+=("${1}")
        shift
    done
    shift

    COTESDEX=cotesdex-valgrind COTESDEXFLAGS=--memcheck \
        CFLAGS="${cflags[@]}" CC=gcc-4.9.2 \
        PKG_CONFIG_PATH="${PKG_CONFIG_PATH}" \
        run "${srcdir[${counter}]}"/configure \
            --bindir="${bindir[${counter}]}" \
            --datadir="${datadir[${counter}]}" \
            --libdir="${libdir[${counter}]}" \
            --prefix="${prefix[${counter}]}" \
            "${args[@]}"

    xmake
    xmake check
    xmake dist
    xmake install

    if test $# -gt 0
    then
        qrun popd
        LD_LIBRARY_PATH="${libdir[${counter}]}:${LD_LIBRARY_PATH}" \
            PATH="${bindir[${counter}]}:${PATH}" \
            PKG_CONFIG_PATH="${pkgconfigdir[${counter}]}:${PKG_CONFIG_PATH}" \
            build "${@}"
        qrun pushd "${builddir[${counter}]}"
    fi

    xmake uninstall

    local left
    left=$(find "${prefix[${counter}]}" -type f | wc -l)
    test ${left} -eq 0 || die "files were left in prefix after uninstall"

    xmake clean
    xmake distclean
    run find -type f -execdir rm {} +
}

# USAGE: build_recursive [features...] -- [args...] -- [next packages]
build_recursive() {
    local feature=${1}
    shift

    if test "${feature}" == "--"
    then
        build_real "${@}"
        return
    fi

    local features=()
    while test "${1}" != "--"
    do
        features+=("${1}")
        shift
    done
    shift

    local args=()
    while test "${1}" != "--"
    do
        args+=("${1}")
        shift
    done
    shift

    build_recursive ${features[@]} -- "${args[@]}" --disable-${feature} -- "${@}"
    build_recursive ${features[@]} -- "${args[@]}" --enable-${feature} -- "${@}"
}

build() {
    counter=$((counter + 1))

    local pkg="${1}"
    shift

    srcdir[${counter}]="${workdir}"/"${pkg}"

    prefix[${counter}]="${workdir}"/"${pkg}"-install
    bindir[${counter}]="${prefix[${counter}]}"/bin
    datadir[${counter}]="${prefix[${counter}]}"/share
    libdir[${counter}]="${prefix[${counter}]}"/lib
    pkgconfigdir[${counter}]="${libdir[${counter}]}"/pkgconfig

    xmkdir "${prefix[${counter}]}"
    xmkdir "${bindir[${counter}]}"
    xmkdir "${datadir[${counter}]}"
    xmkdir "${libdir[${counter}]}"
    xmkdir "${pkgconfigdir[${counter}]}"

    local builddir[${counter}]="${workdir}"/"${pkg}"-build
    xmkdir "${builddir[${counter}]}"
    qrun pushd "${builddir[${counter}]}"

    local features=()
    local tmp=$(mktemp)
    "${srcdir[${counter}]}"/configure --help > "${tmp}"
    for feature in ${probe[@]}
    do
        if grep -F -- "--enable-${feature}" < "${tmp}" > /dev/null
        then
            features+=("${feature}")
        elif grep -F -- "--disable-${feature}" < "${tmp}" > /dev/null
        then
            features+=("${feature}")
        fi
    done
    rm "${tmp}"

    build_recursive ${features[@]} -- -- "${@}"

    qrun popd
    counter=$((counter - 1))
}

main() {
    xmkdir "${aclocaldir}"
    export ACLOCAL_PATH="${aclocaldir}:${ACLOCAL_PATH}"

    for x in gop cotesdex gop-cotesdex
    do
        run git submodule update --init "${x}"
        qrun pushd "${x}"
        run git pull origin master
        git clean -fd
        qrun popd
        try git commit .gitmodules "${x}" -m "update ${x} submodule"
    done

    qrun pushd gop
    run ./autogen.sh
    run find -type f -execdir chmod a-w,g-r,o-rw {} +
    qrun popd

    qrun pushd cotesdex
    run ./autogen.sh
    run find -type f -execdir chmod a-w,g-r,o-rw {} +
    run cp m4/cotesdex.m4 "${aclocaldir}"
    qrun popd

    qrun pushd gop-cotesdex
    run autoreconf -i
    run find -type f -execdir chmod a-w,g-r,o-rw {} +
    qrun popd

    build gop cotesdex gop-cotesdex

    atexit
}

if test "${0}" != "bash" -a "${0}" != "-bash"
then
    main ${@}
fi
